﻿namespace RetailAgent.Logics.Services.Authentication
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        void Authenticate(string username, string password);
    }
}
