﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RetailAgent.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardPage : ContentPage
    {
        public DashboardPage()
        {
            InitializeComponent();
        }
    }
}