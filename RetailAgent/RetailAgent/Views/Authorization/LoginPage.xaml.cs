﻿using RetailAgent.ViewModels.Authorization;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RetailAgent.Views.Authorization
{
    /// <summary>
    /// 
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        /// <summary>
        /// 
        /// </summary>
        private LoginViewModel viewModel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LoginPage()
        {
            InitializeComponent();
            RegisterEvents();

            viewModel = new LoginViewModel();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RegisterEvents()
        {
            EntryUserName.Completed += EntryUserName_Completed;
            EntryPassword.Completed += EntryPassword_Completed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EntryUserName_Completed(object sender, EventArgs e)
        {
            EntryPassword.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EntryPassword_Completed(object sender, EventArgs e)
        {
            if (viewModel.SignInCommand.CanExecute(null))
                viewModel.SignInCommand.Execute(null);
        }
    }
}